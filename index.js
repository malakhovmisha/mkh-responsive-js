const enquire = require('enquire.js');

class Responsive {
    constructor(sizes) {
        this._initSizes(sizes);
    }

    register(query) {
        query = this._translateQuery(query);
        const args = Array.from(arguments);
        args[0] = query;
        return enquire.register.call(enquire, ...args);
    }
    
    unregister(query) {
        query = this._translateQuery(query);
        const args = Array.from(arguments);
        args[0] = query;
        return enquire.unregister.call(enquire, ...args);
    }
    
    _translateQuery(query) {
        const regex = /^([a-z]{1,4})(-left|-right)? ?([a-z]{1,4})?(-left|-right)?$/;
        const match = regex.exec(query);

        if (typeof match[1] === 'undefined') {
            throw 'Passed argument query is not correct';
        }


        if (this._sizes.hasOwnProperty(match[1]) && typeof match[3] === 'undefined') {
            if (match[2] === '-left') {
                const max = this._sizes[match[1]];
                query = `screen and (max-width:${max}px)`;
            } else {
                const min = this._sizes[match[1]] + 1;
                query = `screen and (min-width:${min}px)`;
            }
        } else if (this._sizes.hasOwnProperty(match[1]) && this._sizes.hasOwnProperty(match[3])) {
            const sizes = [
                this._sizes[match[1]], 
                this._sizes[match[3]]
            ];
            sizes.sort((a,b) => a - b);

            sizes[0] += 1;

            query = `screen and (min-width:${sizes[0]}px) and (max-width:${sizes[1]}px)`;
        } else {
            throw 'Passed argument query is not correct';
        }

        return query;
    }

    _initSizes(sizes = {}) {
        const defaultSizes = {
            xs: 0,
            sm: 576,
            md: 768,
            lg: 992,
            xl: 1500
        };

        const filteredSizes = {};

        for (const size in sizes) {
            if (sizes.hasOwnProperty(size)) {
                if (/[a-z]{1,4}/.test(size) && typeof sizes[size] === 'number' && sizes[size] >= 0 && sizes[size] <= 50000) {
                    filteredSizes[size] = sizes[size];
                } else {
                    throw 'Passed argument sizes is not correct';
                }
            }
        }

        this._sizes = Object.keys(sizes).length > 0 ? filteredSizes : defaultSizes;
    }
}

module.exports = Responsive;