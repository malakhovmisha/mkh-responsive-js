const Responsive = require('mkh-responsive');

const responsive = new Responsive();


responsive.register('md', () => {
    console.log('md');
});

responsive.register('lg-right md-right', {
    match: () => {
        console.log('md-right lg-left');
    }, 
    unmatch: () => {
        console.log('left md-left lg-right');
    }
});